import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const firtsBook = {
  img:
    'https://images-na.ssl-images-amazon.com/images/I/51QM2+Zrf2L._AC_SX184_.jpg',
  title: 'See, Touch, Feel: A First Sensory Book',
  author: 'Roger Priddy',
};

const secondBook = {
  img:
    'https://images-na.ssl-images-amazon.com/images/I/41SH-SvWPxL._AC_SX184_.jpg',
  title: 'Clean Code: A Handbook of Agile Software Craftsmanship',
  author: 'Martin Robert C. ',
};

function BookList() {
  return (
    <section className='booklist'>
      <Book
        img={firtsBook.img}
        title={firtsBook.title}
        author={firtsBook.author}
      >
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit
          provident, asperiores labore magnam tempore a! Assumenda corporis nam
          nulla voluptatibus.
        </p>
      </Book>
      <Book
        img={secondBook.img}
        title={secondBook.title}
        author={secondBook.author}
      ></Book>
    </section>
  );
}

const Book = props => {
  const { img, title, author } = props;
  console.log(props);
  return (
    <article className='book'>
      <img src={img} alt='' />
      <h1>{title}</h1>
      <h4>{author}</h4>
      {props.children}
    </article>
  );
};

ReactDOM.render(<BookList />, document.getElementById('root'));
